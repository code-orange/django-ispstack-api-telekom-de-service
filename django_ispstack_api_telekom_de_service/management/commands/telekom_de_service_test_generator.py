from django.core.management.base import BaseCommand

from django_ispstack_api_telekom_de_service.django_ispstack_api_telekom_de_service.services.v3.api_client import *


class Command(BaseCommand):
    help = "Test run for xml generation"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        open(
            "data/django_ispstack_api_telekom_de_service/test/reserviere_pre_order.xml",
            "wt",
        ).write(gen_service_book_pre_order(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/storniere_pre_order.xml.xml",
            "wt",
        ).write(gen_service_cancel_pre_order(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/pruefe_gigabit_verfuegbarkeit.xml",
            "wt",
        ).write(gen_service_check_gbit_availability(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/pruefe_gigabit_verfuegbarkeit_kls.xml",
            "wt",
        ).write(gen_service_check_gbit_availability_kls(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_lokation.xml",
            "wt",
        ).write(gen_service_get_location(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_produkt.xml",
            "wt",
        ).write(gen_service_get_product(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_produkt_bsa.xml",
            "wt",
        ).write(gen_service_get_product_bsa(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_produkt_ftth.xml",
            "wt",
        ).write(gen_service_get_product_ftth(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_termin.xml",
            "wt",
        ).write(gen_service_get_schedule(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_terminverschiebung.xml",
            "wt",
        ).write(gen_service_get_schedule_shift(IspAccessCpe.objects.get(id=1)))

        open(
            "data/django_ispstack_api_telekom_de_service/test/recherchiere_termin_bsa_sdsl_tal.xml",
            "wt",
        ).write(
            gen_service_get_schedule_shift_bsa_sdsl_tal(IspAccessCpe.objects.get(id=1))
        )

        open(
            "data/django_ispstack_api_telekom_de_service/test/lese_pre_order.xml", "wt"
        ).write(gen_service_read_pre_order(IspAccessCpe.objects.get(id=1)))

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
