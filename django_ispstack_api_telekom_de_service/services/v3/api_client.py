import json
from datetime import datetime

import xmltodict
from OpenSSL import crypto
from django.conf import settings

from django_ispstack_access.django_ispstack_access.models import IspAccessCpe


def fetch_cert_details():
    cert_file = settings.TELEKOM_DE_SERVICE_CERT_FILE_PUB
    cert = crypto.load_certificate(crypto.FILETYPE_PEM, open(cert_file, "rb").read())
    issuer = cert.get_issuer()
    issued_by = issuer.CN
    serial = cert.get_serial_number()

    return {
        "issuer": issued_by,
        "serial": serial,
    }


def gen_service_book_pre_order(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_book_pre_order.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:reservierePreOrderRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:reservierePreOrderRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_cancel_pre_order(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_cancel_pre_order.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ser:stornierePreOrderRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["ser:stornierePreOrderRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_check_gbit_availability(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_check_gbit_availability.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:pruefeGigabitVerfuegbarkeitRequest"]["control"]["zeitstempel"] = (
        now.strftime("%Y-%m-%dT%H:%M:%S.000+02:00")
    )

    data["req:pruefeGigabitVerfuegbarkeitRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_check_gbit_availability_kls(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_check_gbit_availability_kls.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:pruefeGigabitVerfuegbarkeitRequest"]["control"]["zeitstempel"] = (
        now.strftime("%Y-%m-%dT%H:%M:%S.000+02:00")
    )

    data["req:pruefeGigabitVerfuegbarkeitRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_location(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_location.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:recherchiereLokationRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:recherchiereLokationRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_product(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_product.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:recherchiereProduktRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:recherchiereProduktRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_product_bsa(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_product_bsa.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:recherchiereProduktRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:recherchiereProduktRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_product_ftth(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_product_ftth.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:recherchiereProduktRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:recherchiereProduktRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_schedule(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_schedule.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:recherchiereTerminRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:recherchiereTerminRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_schedule_shift(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_schedule_shift.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["ser:recherchiereTerminverschiebungRequest"]["control"]["zeitstempel"] = (
        now.strftime("%Y-%m-%dT%H:%M:%S.000+02:00")
    )

    data["ser:recherchiereTerminverschiebungRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_get_schedule_shift_bsa_sdsl_tal(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_get_schedule_shift_bsa_sdsl_tal.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:recherchiereTerminRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:recherchiereTerminRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data


def gen_service_read_pre_order(cpe: IspAccessCpe):
    now = datetime.now()

    data = json.loads(
        open(
            "django_ispstack_api_telekom_de_service/django_ispstack_api_telekom_de_service/services/v3/templates/service_read_pre_order.json",
            "rb",
        ).read()
    )

    cert_details = fetch_cert_details()

    data["req:lesePreOrderRequest"]["control"]["zeitstempel"] = now.strftime(
        "%Y-%m-%dT%H:%M:%S.000+02:00"
    )

    data["req:lesePreOrderRequest"]["control"]["signaturId"] = {
        "issuer": cert_details["issuer"],
        "serial": cert_details["serial"],
    }

    xml_data = xmltodict.unparse(data, pretty=True)

    return xml_data
